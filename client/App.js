import React, { useState, useEffect } from "react";
import { BottomNavigation, Text } from "react-native-paper";
import { StyleSheet, View } from "react-native";
import Events from "./Components/EventComponent";
import People from "./Components/PeopleComponent";
import AsyncStorage from "@react-native-async-storage/async-storage";
import PreApp from "./Components/preapp";
import axios from "axios";
import JWT from "expo-jwt";
import { URL, JWT_SECRET } from "./config.js";

const App = () => {
  // States
  const [nav, setNav] = useState({
    index: 0,
    routes: [
      { key: "people", title: "People", icon: "face-man" },
      { key: "events", title: "Events", icon: "format-list-numbered" },
    ],
  });
  const [allPeople, setAllPeople] = useState(null);
  const [personVisible, setPersonVisible] = useState(false);
  const [presentVisible, setPresentVisible] = useState(false);
  const [current, setCurrent] = useState("");
  const [idx, setIdx] = useState(0);
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  //TOKEN
  const [token, setToken] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);

  const storeData = async (data) => {
    try {
      await AsyncStorage.setItem("token", data);
    } catch (error) {}
  };
  const retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem("token");
      setToken(value);
    } catch (error) {}
  };
  retrieveData();
  const deleteData = async () => {
    try {
      const value = await AsyncStorage.removeItem("token");
      setToken(null);
    } catch (error) {}
  };
  useEffect(() => {
    const verify_token = async () => {
      try {
        if (!token) {
          setIsLoggedIn(false);
        } else {
          axios.defaults.headers.common["Authorization"] = token;
          const response = await axios.post(`${URL}/users/verify_token`);
          let decodedToken = JWT.decode(token, JWT_SECRET);
          setCurrentUser(decodedToken.user);
          return response.data.ok ? letUserIn(token) : logout();
        }
      } catch (error) {
        console.log(error);
      }
    };
    verify_token();
  }, [token]);

  const letUserIn = (token) => {
    storeData(token);
    setIsLoggedIn(true);
  };

  const logout = () => {
    deleteData();
    setIsLoggedIn(false);
  };
  // fetch AllPeople

  const getPeopleFromDb = async () => {
    let url = `${URL}/person/getpeople/${currentUser._id}`;
    try {
      const res = await axios.get(url);
      let parse = res.data.data.map((ele) => ({
        ...ele,
        bday: new Date(ele.bday),
      }));
      setAllPeople([...parse]);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getPeopleFromDb();
  }, [currentUser]);

  // navigation
  const PeopleRoute = () => (
    <People
      setAllPeople={setAllPeople}
      allPeople={allPeople || []}
      personVisible={personVisible}
      setPersonVisible={setPersonVisible}
      current={current}
      setCurrent={setCurrent}
      currentUser={currentUser}
      setIdx={setIdx}
      idx={idx}
      getPeopleFromDb={getPeopleFromDb}
    />
  );
  const EventRoute = () => (
    <Events
      setAllPeople={setAllPeople}
      allPeople={allPeople}
      presentVisible={presentVisible}
      setPresentVisible={setPresentVisible}
      setCurrent={setCurrent}
      current={current}
      idx={idx}
      setIdx={setIdx}
    />
  );
  handleIndexChange = (index) => setNav({ ...nav, index });

  renderScene = BottomNavigation.SceneMap({
    people: PeopleRoute,
    events: EventRoute,
  });

  return !isLoggedIn ? (
    <PreApp isLoggedIn={isLoggedIn} letUserIn={letUserIn} />
  ) : (
    <BottomNavigation
      navigationState={nav}
      onIndexChange={handleIndexChange}
      renderScene={renderScene}
    />
  );
};

export default App;
