import { StyleSheet, View, Pressable, Modal, ScrollView } from "react-native";
import { Text } from "react-native-paper";

function Presents({ allPeople, setPresentVisible, idx }) {
  const renderPresents = () => {
    if (allPeople[idx]?.presents.length !== 0) {
      return allPeople[idx]?.presents.map((ele, i) => {
        return (
          <View key={i} style={styles.present}>
            <Text style={styles.title}>{ele.title}</Text>
            <Text>{ele.description}</Text>
            <Text>{ele.store}</Text>
            <Text>{ele.price}€</Text>
          </View>
        );
      });
    } else {
      return <Text>No presents added yet.</Text>;
    }
  };

  return (
    <View style={styles.modalView}>
      <Text>{allPeople[idx].name} present list:</Text>
      <ScrollView style={styles.scroll}>{renderPresents()}</ScrollView>
      <Pressable
        style={[styles.button, styles.buttonClose]}
        onPress={() => setPresentVisible(false)}
      >
        <Text>Close</Text>
      </Pressable>
    </View>
  );
}
export default Presents;

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  input: {
    height: 40,
    width: "90%",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  icon: {
    width: 70,
    height: 70,
    margin: 8,
  },
  present: {
    margin: 10,
    width: "90%",
    alignItems: "center",
    borderStyle: "solid",
    borderRadius: 20,
    borderWidth: 3,
    borderColor: "black",
    padding: 10,
  },
  title: {
    fontSize: 20,
  },
  scroll: {
    width: "100%",
    height: "50%",
  },
});
