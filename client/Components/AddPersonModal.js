import { StyleSheet, View, Pressable, TextInput, Image } from "react-native";
import { Text } from "react-native-paper";
import { icons } from "./icons";
import React, { useState } from "react";
import { URL } from "../config.js";
import axios from "axios";

function AddPersonModal({ setModalVisible, currentUser, getPeopleFromDb }) {
  const [name, setName] = useState("");
  const [day, setDay] = useState(0);
  const [month, setMonth] = useState(0);
  const [selected, setSelected] = useState(0);
  const year = new Date().getFullYear();

  const addPerson = () => {
    if (day > 31 || month > 12 || !day || !month) {
      return alert("INVALID DATE");
    } else if (!name) {
      return alert("Enter a name");
    } else if (selected == null) {
      return alert("Select an icon");
    } else {
      let birthday = new Date(year, month - 1, day);
      let url = `${URL}/person/add`;
      axios
        .post(url, {
          name: name,
          bday: birthday,
          icon: selected,
          users: currentUser._id,
          presents: null,
        })
        .then((res) => {
          console.log(res.status);
          setModalVisible(false);
          getPeopleFromDb();
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  const renderIcons = () => {
    return icons.map((ele, i) => {
      return (
        <Pressable key={i} onPress={() => select(i)}>
          <Image
            style={selected == i ? styles.iconselected : styles.icon}
            source={ele}
          />
        </Pressable>
      );
    });
  };

  const select = (icon) => {
    setSelected(icon);
  };

  return (
    <View>
      <View style={styles.modalView}>
        <Pressable
          style={[styles.button, styles.buttonClose]}
          onPress={() => setModalVisible(false)}
        >
          <Text>Close</Text>
        </Pressable>
        <Text style={styles.modalText}>Name</Text>
        <TextInput
          style={styles.input}
          onChangeText={(newName) => setName(newName)}
        ></TextInput>
        <Text style={styles.modalText}>Birthday</Text>
        <View style={styles.date}>
          <Text>Day</Text>
          <Text>Month</Text>
        </View>
        <View style={styles.date}>
          <TextInput
            style={styles.dateinput}
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(newDay) => setDay(newDay)}
          ></TextInput>
          <TextInput
            style={styles.dateinput}
            keyboardType="numeric"
            maxLength={2}
            onChangeText={(newMonth) => setMonth(newMonth)}
          ></TextInput>
        </View>
        <Text>Icon</Text>
        <View style={styles.iconcontainer}>{renderIcons()}</View>
        <Pressable
          style={[styles.button, styles.buttonClose]}
          onPress={addPerson}
        >
          <Text>Add</Text>
        </Pressable>
      </View>
    </View>
  );
}
export default AddPersonModal;

const styles = StyleSheet.create({
  date: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around",
  },
  dateinput: {
    width: "40%",
    height: 40,
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  input: {
    height: 40,
    width: "90%",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  iconcontainer: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    flexWrap: "wrap",
  },
  icon: {
    width: 70,
    height: 70,
    margin: 8,
  },
  iconselected: {
    width: 70,
    height: 70,
    margin: 8,
    backgroundColor: "#DEF8FF",
    borderColor: "#424242",
    borderWidth: 3,
    borderRadius: 5,
  },
});
