import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Modal,
  Image,
  ScrollView,
} from "react-native";
import React, { useEffect } from "react";
import { icons } from "./icons";
import Presents from "./PresentsModal";

function Events({
  allPeople,
  setAllPeople,
  presentVisible,
  setPresentVisible,
  setCurrent,
  current,
  idx,
  setIdx,
}) {
  const currentYear = new Date().getFullYear();
  const currentMonth = new Date().getMonth() + 1;
  const currentDay = new Date().getDate();
  const currentDate = new Date(currentYear, currentMonth - 1, currentDay);

  const toRender = [...allPeople];
  const events = toRender.sort((a, b) => a.bday - b.bday);

  useEffect(() => {
    let check = allPeople.find((p) => p.bday < currentDate);
    check &&
      ((allPeople = allPeople.map((person) => {
        if (person.bday < currentDate) {
          return {
            ...person,
            bday: new Date(
              person.bday.setFullYear(person.bday.getFullYear() + 1)
            ),
          };
        } else {
          return person;
        }
      })),
      setAllPeople([...allPeople]));
  }, []);

  const eventRender = () => {
    return events.map((event, idx) => {
      return (
        <Pressable
          key={idx}
          style={styles.eventcontainer}
          onPress={() => {
            setPresentVisible(true);
            setCurrent(event._id);
            setIdx(idx);
          }}
        >
          <Text>{event.name}</Text>
          <Text>
            Birthday{" "}
            {Math.floor(
              (event.bday.getTime() - currentDate.getTime()) /
                (1000 * 60 * 60 * 24)
            ) > 0
              ? "in " +
                Math.floor(
                  (event.bday.getTime() - currentDate.getTime()) /
                    (1000 * 60 * 60 * 24)
                ) +
                " days"
              : `today`}{" "}
          </Text>
          <Image style={styles.icon} source={icons[events[idx]?.icon]} />
        </Pressable>
      );
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.eventTitle}>Next birthdays</Text>
      <ScrollView style={styles.scroll}>{eventRender()}</ScrollView>
      <Modal animationType="slide" transparent={true} visible={presentVisible}>
        <Presents
          allPeople={events}
          current={current}
          setPresentVisible={setPresentVisible}
          presentVisible={presentVisible}
          idx={idx}
        />
      </Modal>
    </View>
  );
}
export default Events;

const styles = StyleSheet.create({
  container: {
    top: "10%",
    flex: 0.9,
    alignItems: "center",
  },
  icon: {
    width: 70,
    height: 70,
    margin: 8,
  },
  eventcontainer: {
    width: "95%",
    alignItems: "center",
    borderStyle: "solid",
    borderWidth: 3,
    borderRadius: 15,
    borderColor: "black",
    marginBottom: "2%",
  },
  scroll: {
    width: "95%",
  },
  eventTitle: {
    fontSize: 30,
    marginBottom: 20,
  },
});
