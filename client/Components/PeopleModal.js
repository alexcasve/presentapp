import {
  StyleSheet,
  View,
  Pressable,
  Modal,
  TextInput,
  Image,
  ScrollView,
} from "react-native";
import { Text } from "react-native-paper";
import { icons } from "./icons";
import React, { useState } from "react";
import { URL } from "../config.js";
import axios from "axios";

function PeopleModal({
  allPeople,
  setPersonVisible,
  current,
  idx,
  getPeopleFromDb,
}) {
  const [presentVisible, setPresentVisible] = useState(false);
  const [present, setPresent] = useState({
    title: "",
    description: "",
    store: "",
    price: 0,
  });

  const addPresent = (_id) => {
    let url = `${URL}/person/presentadd`;
    axios
      .post(url, { person: _id, present: present })
      .then((res) => {
        setPresentVisible(false);
        getPeopleFromDb();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const removePresent = (current, _id) => {
    let url = `${URL}/person/presentremove`;
    axios
      .post(url, { person: current, present: _id })
      .then((res) => {
        getPeopleFromDb();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const deletePerson = (id) => {
    let url = `${URL}/person/delete`;
    axios
      .post(url, {
        _id: id,
      })
      .then((res) => {
        setPersonVisible(false);
        getPeopleFromDb();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const renderPresents = () => {
    return allPeople[idx]?.presents.map((ele, i) => {
      return (
        <View key={i} style={styles.present}>
          <Text style={styles.title}>{ele.title}</Text>
          <Text>{ele.description}</Text>
          <Text>{ele.store}</Text>
          <Text>{ele.price}€</Text>
          <Pressable
            style={[styles.button, styles.buttonRed]}
            onPress={() => {
              removePresent(current, ele._id);
            }}
          >
            <Text>Delete present</Text>
          </Pressable>
        </View>
      );
    });
  };

  return (
    <View style={styles.modalView}>
      <Pressable
        style={[styles.button, styles.buttonClose, styles.closeButton]}
        onPress={() => setPersonVisible(false)}
      >
        <Text>Close</Text>
      </Pressable>
      <Pressable
        style={[styles.button, styles.buttonRed]}
        onPress={() => deletePerson(current)}
      >
        <Text>Delete person</Text>
      </Pressable>
      <Image style={styles.icon} source={icons[allPeople[idx]?.icon]} />
      <Text>{allPeople[idx]?.name}</Text>

      {/* Add present modal */}

      <Modal animationType="slide" transparent={true} visible={presentVisible}>
        <View style={styles.modalView}>
          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => setPresentVisible(!presentVisible)}
          >
            <Text>Close</Text>
          </Pressable>
          <Text>Present title</Text>
          <TextInput
            style={styles.input}
            onChangeText={(newTitle) =>
              setPresent({ ...present, title: newTitle })
            }
          ></TextInput>
          <Text>Description</Text>
          <TextInput
            style={styles.input}
            onChangeText={(newDescription) =>
              setPresent({ ...present, description: newDescription })
            }
          ></TextInput>
          <Text>Store/Website</Text>
          <TextInput
            style={styles.input}
            onChangeText={(newStore) =>
              setPresent({ ...present, store: newStore })
            }
          ></TextInput>
          <Text>Price</Text>
          <TextInput
            onChangeText={(newPrice) =>
              setPresent({ ...present, price: newPrice })
            }
            style={styles.input}
            keyboardType="numeric"
          ></TextInput>

          <Pressable
            style={[styles.button, styles.buttonClose]}
            onPress={() => addPresent(current)}
          >
            <Text>Add</Text>
          </Pressable>
        </View>
      </Modal>

      <Pressable
        style={[styles.button, styles.buttonClose]}
        onPress={() => setPresentVisible(!presentVisible)}
      >
        <Text>Add Present</Text>
      </Pressable>
      <ScrollView style={styles.scroll}>{renderPresents()}</ScrollView>
    </View>
  );
}
export default PeopleModal;

const styles = StyleSheet.create({
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  closeButton: {
    alignSelf: "flex-end",
  },
  input: {
    height: 40,
    width: "90%",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  icon: {
    width: 70,
    height: 70,
    margin: 8,
  },
  present: {
    margin: 10,
    width: "90%",
    alignItems: "center",
    borderStyle: "solid",
    borderRadius: 20,
    borderWidth: 3,
    borderColor: "black",
    padding: 10,
  },
  title: {
    fontSize: 20,
  },
  buttonRed: {
    backgroundColor: "#C90000",
  },
  scroll: {
    width: "100%",
    height: "50%",
  },
});
