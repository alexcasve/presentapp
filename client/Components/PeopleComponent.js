import {
  StyleSheet,
  View,
  Pressable,
  Modal,
  Image,
  ScrollView,
} from "react-native";
import { Text } from "react-native-paper";
import React, { useState } from "react";
import { icons } from "./icons";
import PeopleModal from "./PeopleModal";
import AddPersonModal from "./AddPersonModal";

function People({
  setAllPeople,
  allPeople,
  personVisible,
  setPersonVisible,
  current,
  setCurrent,
  currentUser,
  setIdx,
  idx,
  getPeopleFromDb,
}) {
  console.log(allPeople);
  const [modalVisible, setModalVisible] = useState(false);
  const renderPeople = () => {
    if (allPeople) {
      return allPeople.map((ele, i) => {
        return (
          <Pressable
            key={i}
            style={styles.add}
            onPress={() => {
              setCurrent(ele._id);
              setIdx(i);
              setPersonVisible(true);
            }}
          >
            <Image style={styles.icon} source={icons[ele.icon]} />
            <Text>{ele.name}</Text>
          </Pressable>
        );
      });
    }
  };

  return (
    <View style={styles.container}>
      {/* Add person modal */}
      <Modal animationType="slide" transparent={true} visible={modalVisible}>
        <AddPersonModal
          setModalVisible={setModalVisible}
          modalVisible={modalVisible}
          setAllPeople={setAllPeople}
          allPeople={allPeople}
          currentUser={currentUser}
          getPeopleFromDb={getPeopleFromDb}
        />
      </Modal>

      {/* Person modal */}

      <Modal animationType="slide" transparent={true} visible={personVisible}>
        <PeopleModal
          allPeople={allPeople}
          setAllPeople={setAllPeople}
          current={current}
          personVisible={personVisible}
          setPersonVisible={setPersonVisible}
          idx={idx}
          getPeopleFromDb={getPeopleFromDb}
        />
      </Modal>

      {/* end of modal */}
      {renderPeople()}
      <Pressable style={[styles.add]} onPress={() => setModalVisible(true)}>
        <Image style={styles.icon} source={require("../assets/add.png")} />
      </Pressable>
    </View>
  );
}
export default People;

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    flexDirection: "row",
    flex: 0.8,
    top: 50,
  },
  date: {
    flexDirection: "row",
    width: "90%",
    justifyContent: "space-around",
  },
  dateinput: {
    width: "40%",
    height: 40,
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  add: {
    width: 130,
    height: 130,
    alignItems: "center",
    justifyContent: "center",
    padding: 1,
    marginBottom: 10,
    borderStyle: "solid",
    borderWidth: 4,
    borderRadius: 20,
    borderColor: "#424242",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  input: {
    height: 40,
    width: "90%",
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  icon: {
    width: 70,
    height: 70,
    margin: 8,
  },
  iconhighlight: {
    width: 80,
    height: 80,
    margin: 8,
    borderColor: "red",
    borderWidth: 3,
  },
  iconcontainer: {
    justifyContent: "center",
    flexDirection: "row",
    width: "100%",
    flexWrap: "wrap",
  },
});
