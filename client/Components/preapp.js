import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  SafeAreaView,
  Pressable,
} from "react-native";
import { useState, useEffect } from "react";
import axios from "axios";
import JWT from "expo-jwt";
import { URL, JWT_SECRET } from "../config";

function PreApp({ isLoggedIn, letUserIn }) {
  const [form, setValues] = useState({
    email: "",
    password: "",
    password2: "",
  });
  const [message, setMessage] = useState("");

  const register = async (e) => {
    try {
      const response = await axios.post(`${URL}/users/register`, {
        email: form.email,
        password: form.password,
        password2: form.password2,
      });
      setMessage(response.data.message);
      setTimeout(() => {
        setMessage("");
      }, 5000);
      if (response.data.ok) {
        console.log("Server says sign up ok");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const login = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post(`${URL}/users/login`, {
        email: form.email,
        password: form.password,
      });
      setMessage(response.data.message);
      if (response.data.ok) {
        let decodedToken = JWT.decode(response.data.token, JWT_SECRET);
        console.log(
          "Email extracted from the JWT token after login: ",
          decodedToken.user
        );
        setTimeout(() => {
          letUserIn(response.data.token);
        }, 2000);
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <SafeAreaView style={styles.container}>
      {
        <View style={styles.container}>
          <Text>REGISTER</Text>
          <TextInput
            style={styles.input}
            placeholder={"Enter email"}
            onChangeText={(text) =>
              setValues({ ...form, email: text.toLowerCase().trim() })
            }
            onSubmitEditing={(event) =>
              setValues({
                ...form,
                email: event.nativeEvent.text.toLowerCase().trim(),
              })
            }
          />
          <TextInput
            style={styles.input}
            placeholder={"Enter password"}
            secureTextEntry={true}
            onChangeText={(text) =>
              setValues({ ...form, password: text.toLowerCase().trim() })
            }
            onSubmitEditing={(event) =>
              setValues({
                ...form,
                password: event.nativeEvent.text.toLowerCase().trim(),
              })
            }
          />
          <TextInput
            style={styles.input}
            placeholder={"Confirm password"}
            secureTextEntry={true}
            onChangeText={(text) =>
              setValues({ ...form, password2: text.toLowerCase().trim() })
            }
            onSubmitEditing={(event) =>
              setValues({
                ...form,
                password2: event.nativeEvent.text.toLowerCase().trim(),
              })
            }
          />
          <Text>
            {form.password2 &&
              form.password !== form.password2 &&
              "Password should match..."}
          </Text>
          <Pressable
            onPress={register}
            style={[styles.button, styles.buttonClose]}
          >
            <Text>Sign Up</Text>
          </Pressable>
          <Text>{message}</Text>
          <Text>LOGIN</Text>
          <TextInput
            style={styles.input}
            placeholder={"Enter email"}
            onChangeText={(text) =>
              setValues({ ...form, email: text.toLowerCase().trim() })
            }
            onSubmitEditing={(event) =>
              setValues({
                ...form,
                email: event.nativeEvent.text.toLowerCase().trim(),
              })
            }
          />
          <TextInput
            style={styles.input}
            placeholder={"Enter password"}
            secureTextEntry={true}
            onChangeText={(text) =>
              setValues({ ...form, password: text.toLowerCase().trim() })
            }
            onSubmitEditing={(event) =>
              setValues({
                ...form,
                password: event.nativeEvent.text.toLowerCase().trim(),
              })
            }
          />
          <Pressable
            onPress={login}
            style={[styles.button, styles.buttonClose]}
          >
            <Text>Log In</Text>
          </Pressable>
          <Text>{message}</Text>
          <StatusBar style="auto" />
        </View>
      }
    </SafeAreaView>
  );
}

export default PreApp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    top: 100,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  input: {
    height: 40,
    width: 250,
    margin: 7,
    paddingLeft: 5,
    borderColor: "black",
    borderRadius: 10,
    borderWidth: 2,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
});
