export const icons = [
  require("../assets/icons/baby.png"),
  require("../assets/icons/daughter.png"),
  require("../assets/icons/son.png"),
  require("../assets/icons/mother.png"),
  require("../assets/icons/wifes-father.png"),
  require("../assets/icons/old-woman.png"),
  require("../assets/icons/grandfather.png"),
];
