1- To start the app, npm install in client and server folder.
2- In the client, execute the command: expo start
3- In the server, execute the command: nodemon
4- In your browser, open localhost/19002
5- Install Expo Go on your device
6- Scan the QR code that's on step 4.
7- Register your account.
8- You are good to go!
