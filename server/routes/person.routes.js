const router = require("express").Router();
const controller = require("../controllers/person.controllers");

router.post("/add", controller.add);
router.post("/delete", controller.splice);
router.post("/presentadd", controller.addpresent);
router.post("/presentremove", controller.removepresent);
router.get("/getpeople/:_id", controller.getpeople);

module.exports = router;
