const User = require("../models/users.models");
const argon2 = require("argon2");
const jwt = require("jsonwebtoken");
const validator = require("validator");
const jwt_secret = process.env.JWT_SECRET;

const register = async (req, res) => {
  const { email, password, password2 } = req.body;
  if (!email || !password || !password2)
    return res.json({ ok: false, message: "All fields required" });
  if (password !== password2)
    return res.json({ ok: false, message: "Passwords must match" });
  if (password.length < 8)
    return res.json({
      ok: false,
      message: "Password should have 8 or more characters",
    });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "Invalid email" });
  try {
    const user = await User.findOne({ email });
    if (user) return res.json({ ok: false, message: "User already exists" });
    const hash = await argon2.hash(password);
    console.log("hash ==>", hash);
    const newUser = {
      email,
      password: hash,
    };
    await User.create(newUser);
    res.json({ ok: true, message: "Successfully registered" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res.json({ ok: false, message: "All field are required" });
  if (!validator.isEmail(email))
    return res.json({ ok: false, message: "invalid email provided" });
  try {
    const user = await User.findOne({ email });
    if (!user) return res.json({ ok: false, message: "user not found" });
    const match = await argon2.verify(user.password, password);
    if (match) {
      const token = jwt.sign({ user }, jwt_secret); // without {expiresIn:'365d'} it will last forever
      res.json({ ok: true, message: "welcome back", token, email });
    } else return res.json({ ok: false, message: "invalid password provided" });
  } catch (error) {
    res.json({ ok: false, error });
  }
};

const verify_token = (req, res) => {
  console.log(req.headers.authorization);
  const token = req.headers.authorization;
  jwt.verify(token, jwt_secret, (err, succ) => {
    err
      ? res.json({ ok: false, message: "something went wrong" })
      : res.json({ ok: true, succ });
  });
};

module.exports = { register, login, verify_token };
