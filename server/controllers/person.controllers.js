const Person = require("../models/person.models");

const add = async (req, res) => {
  let { name, bday, icon, users } = req.body;
  bday = new Date(bday);
  try {
    const newPerson = {
      name: name,
      bday: bday,
      icon: icon,
      users: [{ id: users }],
      presents: [],
    };
    let pers = await Person.create(newPerson);
    console.log(typeof pers.bday);
    res.send({ ok: true, message: "New person added", pers });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const splice = async (req, res) => {
  const { _id } = req.body;
  try {
    await Person.deleteOne({ _id });
    res.send({ ok: true, message: "Person deleted" });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const addpresent = async (req, res) => {
  const { person, present } = req.body;
  try {
    await Person.updateOne(
      { _id: person },
      {
        $push: {
          presents: present,
        },
      }
    );
    res.send({ ok: true, message: "present added" });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const removepresent = async (req, res) => {
  const { person, present } = req.body;
  try {
    await Person.updateOne(
      { _id: person },
      { $pull: { presents: { _id: present } } }
    );
    res.send({ ok: true, message: "Present deleted" });
  } catch (error) {
    res.send({ ok: false, error });
  }
};

const getpeople = async (req, res) => {
  let { _id } = req.params;
  try {
    let found = await Person.find({ users: { $elemMatch: { id: _id } } });
    res.json({ ok: true, data: found });
  } catch (error) {
    res.send({ ok: false });
  }
};

module.exports = { add, splice, addpresent, removepresent, getpeople };
