const mongoose = require("mongoose");

const personSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    bday: { type: Date, required: true },
    icon: { type: Number, required: true },
    users: [{ id: { type: String, required: true } }],
    presents: [
      {
        title: { type: String, required: true },
        description: { type: String, required: true },
        store: { type: String, required: true },
        price: { type: Number, required: true },
      },
    ],
  },
  { strictQuery: false }
);

module.exports = mongoose.model("persons", personSchema);
